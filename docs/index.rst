.. DARIAH-DE Data Federation Architecture (DFA) documentation master file

DARIAH-DE Forschungsdaten-Föderationsarchitektur (DFA)
======================================================
Die DARIAH-DE Forschungsdaten-Föderationsarchitektur (DFA) beschreibt ein Gesamtkonzept des Forschungsdatenmanagements und umfasst mehrere Softwarekomponenten, die den reproduzierbaren und nachhaltigen Umgang mit Forschungsdaten erleichtern. 

.. dfa_architektur:
.. figure:: ./pics/dfa_architektur.png

Konzeptionell ist die DFA ist in Schichten organisiert. Sie ordnet ihre Komponenten der Datenschicht, Föderationsschicht und Diensteschicht zu und kann in jeder dieser Schichten jederzeit erweitert werden.

 * Auf Ebene der *Datenschicht* werden Forschungsdaten gespeichert und verwaltet
 * Die Komponenten der *Föderationsschicht* haben eine nachweisende und deskriptive Funktion und bieten insbesondere Schnittstellen für die integrative Betrachtung von Forschungsdaten
 * Die *Diensteschicht* umfasst schließlich diejenigen Softwaresystem, die auf Basis untergeordneter Schichten einen weiterführenden Nutzen für ihre Anwenderinnen und Anwender generieren.

Folgend finden Sie die Links zu den derzeitigen DFA Diensten.

DARIAH-DE Repository
====================
 * Produktivsystem: https://repository.de.dariah.eu
 * Testinstanz: https://dhrepworkshop.de.dariah.eu
 * Dokumentation: https://repository.de.dariah.eu/doc
 * Source Code: https://repository.de.dariah.eu/doc/services/sourcecode.html

DARIAH-DE Collection Registry (CR)
==================================
 * Produktivsystem: https://colreg.de.dariah.eu
 * Testinstanz: https://dfatest.de.dariah.eu/colreg-ui
 * Dokumentation: https://dfa.de.dariah.eu/doc/colreg
 * Source Code: https://gitlab.com/DARIAH-DE/colreg

DARIAH-DE Data Modeling Environment (DME)
=========================================
 * Produktivsystem: https://dme.de.dariah.eu
 * Testinstanz: https://dfatest.de.dariah.eu/dme
 * Dokumentation: https://dfa.de.dariah.eu/doc/dme
 * Source Code: https://gitlab.rz.uni-bamberg.de/dariah/dme

DARIAH-DE Generische Suche (GS)
===============================
 * Produktivsystem: https://search.de.dariah.eu
 * Testinstanz: https://dfatest.de.dariah.eu/search
 * Dokumentation: https://dfa.de.dariah.eu/doc/search
 * Source Code: https://gitlab.rz.uni-bamberg.de/dariah/search

.. toctree::
   :maxdepth: 1
